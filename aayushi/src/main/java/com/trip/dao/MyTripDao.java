package com.trip.dao;
import java.util.List;

import com.trip.model.CustomerModel;
import com.trip.model.CustomerStatusModel;
import com.trip.model.TrainDetailModel;
public interface MyTripDao 
{
	public String login(CustomerModel customerModel);
	List<TrainDetailModel>show(String id);
	public void insert(CustomerStatusModel customerStatusModel);
	List<CustomerStatusModel>show2();
}

package com.trip.dao.impl;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.activation.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import com.trip.model.CustomerModel;
import com.trip.model.CustomerStatusModel;
import com.trip.model.TrainDetailModel;
public class MyTripDaoImpl
{
	DataSource dataSource;
	@Autowired
	JdbcTemplate jdbcTemplate;
	//for login
	public String login(CustomerModel customerModel) 
	{
		String sql="select * from customedetail where id='"+customerModel.getId()+"'&& password='"+customerModel.getPassword()+"'";
		List<CustomerModel> customerModel2= jdbcTemplate.query(sql, new VerifyUserMapper());
		String flag = null;
		if(customerModel2.size()>0)
		{
		String dbId=customerModel2.get(0).getId();
		String dbpassword=customerModel2.get(0).getPassword();
		if(customerModel.getId().equals(dbId)&&customerModel.getPassword().equals(dbpassword))
			{
				return flag="true";
			}
		}
		else
		{
			return flag="false";
		}
		return flag;
	}
	class VerifyUserMapper implements RowMapper<CustomerModel>
	{
		public CustomerModel mapRow(ResultSet rs, int arg1)throws SQLException
		{
			CustomerModel customerModel =new CustomerModel();	
			customerModel.setId(rs.getString("Id"));
			customerModel.setPassword(rs.getString("password"));		
			return customerModel;
		}
	}
	public DataSource getDataSource() {
		return dataSource;
	}


	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}


	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}


	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	//for display train list
	public List<TrainDetailModel> show(String id)
	{
		String sql="select * from traindetail where active='true'";
		List<TrainDetailModel> trainList= jdbcTemplate.query(sql, new VerifyUserMapper1());
		return trainList;
	}
	class VerifyUserMapper1 implements RowMapper<TrainDetailModel>
	{
		public TrainDetailModel mapRow(ResultSet rs, int arg1)throws SQLException
		{
			TrainDetailModel trainDetailModel =new TrainDetailModel();	
			trainDetailModel.settId(rs.getString("tId"));
			trainDetailModel.setFrom(rs.getString("from"));
			trainDetailModel.setTo(rs.getString("to"));
			trainDetailModel.setPrice(rs.getString("price"));
			return trainDetailModel;
		}
	}
	//for insert customerstatus
	public void insert(CustomerStatusModel customerStatusModel)
	{
		String sql="insert into customerstatus(cId,price)values('"+customerStatusModel.getcId()+"+"+customerStatusModel.getCheck()+"','"+customerStatusModel.getPrice()+"')";
		jdbcTemplate.update(sql);	
	}

	//for display customerstatus
	public List<CustomerStatusModel> show2() 
	{
		String sql="select * from customerstatus";
		List<CustomerStatusModel> custList= jdbcTemplate.query(sql, new VerifyUserMapper2());
		return custList;
	}
	class VerifyUserMapper2 implements RowMapper<CustomerStatusModel>
	{
		public CustomerStatusModel mapRow(ResultSet rs, int arg1)throws SQLException
		{
			CustomerStatusModel customerStatusModel =new CustomerStatusModel();	
			customerStatusModel.setcId(rs.getString("cId"));
			customerStatusModel.setPrice(rs.getString("price"));
			return customerStatusModel;
		}
	}

}
		
	
	

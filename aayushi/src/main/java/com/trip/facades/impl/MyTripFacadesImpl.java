package com.trip.facades.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.trip.facades.MyTripFacades;
import com.trip.model.CustomerModel;
import com.trip.model.CustomerStatusModel;
import com.trip.model.TrainDetailModel;
import com.trip.services.impl.MyTripServicesImpl;
public class MyTripFacadesImpl implements MyTripFacades
{
	@Autowired
	MyTripServicesImpl myTripServicesImpl;
	@Override
	public String login(CustomerModel customerModel) 
	{
		
		return myTripServicesImpl.login(customerModel) ;
	}
	@Override
	public List<TrainDetailModel> show(String id)
	{
		
		return myTripServicesImpl.show(id);
	}
	@Override
	public void insert(CustomerStatusModel customerStatusModel) {
		
		System.out.println("in facades");
		System.out.println(customerStatusModel.getCheck());
		System.out.println(customerStatusModel.getcId());
		myTripServicesImpl.insert(customerStatusModel);
		
	}
	@Override
	public List<CustomerStatusModel> show2() 
	{
		return myTripServicesImpl.show2();
	}

	
}

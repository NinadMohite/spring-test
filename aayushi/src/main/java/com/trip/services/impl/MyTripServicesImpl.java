package com.trip.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.trip.dao.impl.MyTripDaoImpl;
import com.trip.model.CustomerModel;
import com.trip.model.CustomerStatusModel;
import com.trip.model.TrainDetailModel;
import com.trip.services.MyTripServices;

public class MyTripServicesImpl implements MyTripServices
{
	@Autowired
	MyTripDaoImpl myTripDaoImpl;
	@Override
	public String login(CustomerModel customerModel) 
	{
		return myTripDaoImpl.login(customerModel) ;
	}
	@Override
	public List<TrainDetailModel> show(String id) {
		
		return myTripDaoImpl.show(id);
	}
	@Override
	public void insert(CustomerStatusModel customerStatusModel) {
		
		System.out.println("in services");
		System.out.println(customerStatusModel.getCheck());
		System.out.println(customerStatusModel.getcId());
		 myTripDaoImpl.insert(customerStatusModel);
	}
	@Override
	public List<CustomerStatusModel> show2() {
		return myTripDaoImpl.show2();
	}

}

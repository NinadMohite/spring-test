package com.trip.services;
import java.util.List;

import com.trip.model.CustomerModel;
import com.trip.model.CustomerStatusModel;
import com.trip.model.TrainDetailModel;
public interface MyTripServices 
{
	public String login(CustomerModel customerModel) ;
	List<TrainDetailModel>show(String id);
	public void insert(CustomerStatusModel customerStatusModel);
	List<CustomerStatusModel>show2();
}

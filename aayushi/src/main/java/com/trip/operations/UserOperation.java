package com.trip.operations;
import java.util.List;

import javax.activation.DataSource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.swing.plaf.synth.SynthSpinnerUI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.trip.facades.impl.MyTripFacadesImpl;
import com.trip.model.CustomerModel;
import com.trip.model.CustomerStatusModel;
import com.trip.model.TrainDetailModel;
@Controller
public class UserOperation
{
	DataSource dataSource; 
	JdbcTemplate jdbcTemplate;
	@Autowired
	MyTripFacadesImpl myTripFacadesImpl;
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String homePage()
	{
	 return "home";
	}
	@RequestMapping(value = "/newfile", method = RequestMethod.GET)
	public String newfilePage()
	{
	 return "newfile";
	}
	//for Login
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginVerify(@ModelAttribute("CustomerModel") CustomerModel customerModel,HttpServletRequest request) 
	{
		String flag = myTripFacadesImpl.login(customerModel);
		if (flag=="true")
		{
			HttpSession session = request.getSession();
			session.setAttribute("Id",customerModel.getId());
			return "redirect:display";
		}
		else
		{
			return "redirect:home";
		}
	}
	//for Display
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView displayuser(HttpServletRequest request,String id)
	{
		HttpSession session = request.getSession();
		String Id=(String) session.getAttribute("Id");
		List<TrainDetailModel> list=myTripFacadesImpl.show(Id);
		for(TrainDetailModel l:list)
		{
		
		}
		return new ModelAndView("trainlist", "list", list);
	}
	//for insert UserStatus
	@RequestMapping(value = "/userstatus", method = RequestMethod.POST)
	public String userinfo(@ModelAttribute("CustomerStatusModel") CustomerStatusModel customerStatusModel,HttpServletRequest request)
	{
		myTripFacadesImpl.insert(customerStatusModel);
		return "redirect:display2";
	}
	//for Display UserStatus
	@RequestMapping(value = "/display2", method = RequestMethod.GET)
	public ModelAndView dis()
	{
		
		List<CustomerStatusModel> list=myTripFacadesImpl.show2();
		for(CustomerStatusModel l:list)
		{
		
		}
		return new ModelAndView("NewFile", "list", list);
	}
}

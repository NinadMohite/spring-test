package com.trip.model;
//for customerstatus
public class CustomerStatusModel 
{
	String cId; //customerId
	String tId; //trainId
	String price;
	String check; //value of checkbox
	public String getcId()
	{
		return cId;
	}
	public void setcId(String cId) 
	{
		this.cId = cId;
	}
	public String gettId()
	{
		return tId;
	}
	public void settId(String tId)
	{
		this.tId = tId;
	}
	public String getPrice()
	{
		return price;
	}
	public void setPrice(String price)
	{
		this.price = price;
	}
	public String getCheck() 
	{
		return check;
	}
	public void setCheck(String check)
	{
		this.check = check;
	}

}

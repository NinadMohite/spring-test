package com.project.Contoller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.project.Facades.LoginFacades;


@Controller
public class LoginController 
{
@Autowired
LoginFacades loginfacades;



@RequestMapping(value = "/Regi")
public ModelAndView Login()
{
	return new ModelAndView("Regi");
}
@RequestMapping(value = "/Regiuser", method = RequestMethod.POST)
public ModelAndView regiUser(@ModelAttribute("user") Login user)
{
System.out.println("In regiuser controller");
System.out.println(user.getMailId());	
System.out.println(user.getPassword());	


loginfacades.regiUser(user);

return new ModelAndView ("display", "Key2", user.getMailId()); 
}


@RequestMapping("/Login")
public ModelAndView method1()
{
	  return new ModelAndView("Login");
}
@RequestMapping(value ="/LoginValidate", method = RequestMethod.POST) 
public ModelAndView validateUser(@ModelAttribute("Login")Login login,HttpServletRequest request)

{
			HttpSession session = request.getSession();
			//Login user = null;
			System.out.println("In login controller");
			session.setAttribute("MailId",login.getMailId());
		  String type=loginfacades.validateUser(login);
		  if(type=="verify")
		  {
			  return new ModelAndView("display","key","success");
		  }
		  else
		  {
			  System.out.println("kkkkkkkkk");
			  return new ModelAndView("Regi","key","unsuccessful");
		  }
}
	
//To display the details of train


@RequestMapping("/traindetails")
public ModelAndView view()
{
	  return new ModelAndView("traindetails");
}
@RequestMapping(value ="/traininformation",method=RequestMethod.GET)
public ModelAndView displayTrain()
{
	System.out.println("in controller" );
	List<Login> list=loginfacades.displayTrain();
	System.out.println("logincontroller displayuser");
	for(Login user:list)
	{
		//System.out.println(user);
		System.out.println("Train : "+user.getTrain());
		System.out.println("To : "+user.getTo());
		System.out.println("From : "+user.getFrom());
		System.out.println("Active : "+user.getActive());
		System.out.println("Price : "+user.getPrice());
		
	}
	return new ModelAndView ("traindetails", "list" ,list);
}

@RequestMapping("/NEW")
public ModelAndView view1()
{
	  return new ModelAndView("NEW");
}

//
@RequestMapping(value ="/ticekt/{custId}/{Price}",method=RequestMethod.GET)
public ModelAndView ticketdetails(@PathVariable("custId") Integer custId ,@PathVariable("Price") String Price,HttpServletRequest request)
{
	System.out.println("IN ticket");
	HttpSession session = request.getSession();
	String mailId=(String) session.getAttribute("MailId");
	Login trainModel=new Login();
	//Integer custId;
	// custId = null;
	trainModel.setCustId(custId);
	loginfacades.ticketdetails(trainModel,mailId);
return null;
}
@RequestMapping(value ="/Mybook",method=RequestMethod.GET)

public ModelAndView myInfo(HttpServletRequest request)
{
	
	HttpSession session = request.getSession();
	String cName=(String) session.getAttribute("cName");
	System.out.println("in controller");
	List<Login> list=loginfacades.myInfo();
	
	return new ModelAndView("userDetails","list",list);

}}
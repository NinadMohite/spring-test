package com.project.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.project.Contoller.Login;
import com.project.Dao.LoginDao;

public class LoginServicesimpl implements LoginServices
{
@Autowired
LoginDao loginDao;

@Override
public String validateUser(Login login) {
	return loginDao.validateUser(login);
}



@Override
public List<Login> displayTrain() {
	// TODO Auto-generated method stub
	return loginDao.displayTrain();
}



@Override
public boolean regiUser(Login user) {
	
	return loginDao.regiUser(user);
}



@Override
public Object ticketdetails(Login trainModel, Integer custId) {
	return loginDao.ticketdetails(trainModel,custId);
}



@Override
public List<Login> myInfo() {
	return loginDao.myInfo();
}



//@Override
//public void ticketDetails(Login trainModel, String cName) {
//	return loginDao.ticketDetails(trainModel,cName);
//	
//}


}

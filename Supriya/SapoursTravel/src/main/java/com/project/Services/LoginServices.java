package com.project.Services;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import com.project.Contoller.Login;

public interface LoginServices {

	
	public String validateUser(Login login);
	public List<Login> displayTrain();
	public boolean regiUser(Login user);
	public Object ticketdetails(Login trainModel, Integer custId);
	public List<Login> myInfo();
}

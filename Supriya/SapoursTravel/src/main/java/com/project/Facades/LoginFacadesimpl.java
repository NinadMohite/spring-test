package com.project.Facades;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.project.Contoller.Login;
import com.project.Services.LoginServices;

public class LoginFacadesimpl implements LoginFacades
{
	@Autowired
	 LoginServices loginservices;

	@Override
	public String validateUser(Login login) {
		return loginservices.validateUser(login);
	}

	
	@Override
	public List<Login> displayTrain() {
		
		return loginservices.displayTrain();
	}


	@Override
	public boolean regiUser(Login user) {
		return loginservices.regiUser(user);
	}



	public void ticketdetails(Login trainModel, Integer custId) {
		
		loginservices.ticketdetails(trainModel,custId);
	}


	@Override
	public void ticketdetails(Login trainModel, String mailId) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<Login> myInfo() {
		return loginservices.myInfo();
	}


	@Override
	public void ticketDetails(Login trainModel, Integer custId) {
		// TODO Auto-generated method stub
		
	}


	
	


//	@Override
//	public void ticketDetails(Login trainModel, String cName) {
//		
//		return loginservices.ticketDetails(trainModel,cName);
//	}


}

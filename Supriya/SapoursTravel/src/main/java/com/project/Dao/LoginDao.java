package com.project.Dao;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import com.project.Contoller.Login;

public interface LoginDao
{
	
	public String validateUser(Login login);

	
	public List<Login> displayTrain();
	//public List<Login> displayTrain();
	public boolean regiUser(Login user);


	public Object ticketdetails(Login trainModel, Integer custId);

	public List<Login> myInfo();

	//public void ticketDetails(Login trainModel, String cName); 
}

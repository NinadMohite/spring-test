<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<table>
<tr><th>Location</th><th>From</th><th>To</th><th>Status</th><th>Price</th></tr>


<c:forEach items="${list}" var="trip">
<tr>

<td>Location    :<input type="text" value="${trip.location}"name="location"></td> 
<td>From	    :<input type="text" value="${trip.from}"name="from"></td> 
<td>To		    :<input type="text" value="${trip.to}"name="to"></td> 
<td>Status	    :<input type="text" value="${trip.status}"name="status"></td> 
<td>Price	    :<input type="text" value="${trip.price}"name="price"></td> 

</tr>
</c:forEach>
</table>

</body>
</html>
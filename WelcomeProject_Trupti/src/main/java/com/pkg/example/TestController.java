package com.pkg.example;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pkg.models.LoginModel;

@Controller
public class TestController {
	
	
	@RequestMapping("/test")
	public String hello(Model model) {
		System.out.println("test");
		model.addAttribute("loginModel", new LoginModel());
		return "login";
	}
	
	@RequestMapping(value="/test",method=RequestMethod.POST )
	public String hello1(Model model) {
		model.addAttribute("loginModel", new LoginModel());
		return "login";
	}

}

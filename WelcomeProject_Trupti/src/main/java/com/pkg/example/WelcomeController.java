package com.pkg.example;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.pkg.facades.impl.TripFacadesImpl;
import com.pkg.models.LoginModel;

@Controller
public class WelcomeController {

	@RequestMapping("/welcome")
	public String hello(Model model) {
		model.addAttribute("loginModel", new LoginModel());
		return "welcome";
	}

	@Autowired
	TripFacadesImpl loginFacadesClass;

	
	//for registration

	@RequestMapping(value = "/welcomecontroller1", method = RequestMethod.POST)
	public ModelAndView registeruser(@ModelAttribute("loginModel") LoginModel loginModel) {
		System.out.println("test model");
		loginFacadesClass.registeruser(loginModel);
		return new ModelAndView("error", "key2", "Registered Successfully!");
	}


}

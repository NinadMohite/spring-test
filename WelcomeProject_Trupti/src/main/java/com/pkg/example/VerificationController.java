package com.pkg.example;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.pkg.facades.impl.TripFacadesImpl;
import com.pkg.models.CustomerStatusModel;
import com.pkg.models.LoginModel;
import com.pkg.models.TripModel;

@Controller
public class VerificationController {
	
	@Autowired
	TripFacadesImpl loginFacadesClass;
	
	@RequestMapping("/login")
	public String hello(Model model) {
		model.addAttribute("loginModel", new LoginModel());
		return "login";
	}
	
	//for verification
	
	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	public ModelAndView verifyuser(@ModelAttribute("loginModel") LoginModel loginModel, HttpServletRequest request) {

		boolean flag = loginFacadesClass.verifyuser(loginModel);
		
		//return new ModelAndView("error", "key2", "Login Successfully!");
		if (flag == true) {
			HttpSession session = request.getSession();
			session.setAttribute("username",loginModel.getUsername());
			return new ModelAndView("operations", "login", "Login Successfully");
		} else {
			return new ModelAndView("error", "nologin", "Please register first!");
		}
	}
	
	//To display all available tickets 
	
	@RequestMapping(value = "/bookticket", method = RequestMethod.GET)
	public ModelAndView displaytickets() {
		//System.out.println(loginModel.getUsername());
		List<TripModel> list = loginFacadesClass.displaytickets();
		System.out.println(list.size());
		for(TripModel l:list){
			System.out.println(l.getLocation());
		}
		//System.out.println("pass to facades");
		
		return new ModelAndView("bookticket", "list", list);
	}
	
	
	
	@RequestMapping(value = "/tripdisplay", method = RequestMethod.GET)
	public ModelAndView display() {
		//System.out.println(loginModel.getUsername());
		List<TripModel> list = loginFacadesClass.display();
		System.out.println(list.size());
		for(TripModel l:list){
			System.out.println(l.getLocation());
		}
		//System.out.println("pass to facades");
		
		return new ModelAndView("tripdisplay", "list", list);
	}
	
	//for buy tickets
	
	@RequestMapping(value = "/updated/{location}/{price}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable("location") String location, @PathVariable("price") String price, HttpServletRequest request)
	{	
		
		HttpSession session = request.getSession();
		String mailId=(String) session.getAttribute("username");
		
		TripModel tripModel = new TripModel();
		tripModel.setLocation(location);
		tripModel.setPrice(price);
		
		loginFacadesClass.update(tripModel,mailId);
	
		return new ModelAndView("bookticket", "key2", "Ticket Buy!!");
	}
	
	// To show user purchase tickets and addition of ticket prices
	
	@RequestMapping(value = "/userstatus", method = RequestMethod.GET)
	public String userpurchasedisplay(String username,HttpServletRequest request, Model model) {
		System.out.println("pass to facades");
		
		HttpSession session = request.getSession();
		String username1=(String)session.getAttribute("mailId");
		
		List<CustomerStatusModel> list = loginFacadesClass.userpurchasedisplay(username1);
		System.out.println("test"+list.size());
		int totalPrice=0;
		//int totalBooks=0;
		int temp=0;
		for(CustomerStatusModel l:list)
		{
			//System.out.println("test"+l.getBookPrice());
			totalPrice = totalPrice+Integer.parseInt(l.getPrice());
		}
		model.addAttribute("list", list);
	
		model.addAttribute("totalPrice", totalPrice);
		
		//model.addAttribute("totalBooks", list.size());
		return "userstatus";
	}
	
	

}



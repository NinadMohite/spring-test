package com.pkg.dao.impl;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.activation.DataSource;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;


import com.pkg.dao.TripDao;
import com.pkg.models.CustomerStatusModel;
import com.pkg.models.LoginModel;
import com.pkg.models.TripModel;


public class TripDaoImpl implements TripDao {
	
	
DataSource dataSource;
	 
JdbcTemplate jdbcTemplate;

		 
		public void registeruser(LoginModel loginmodel) 
		{		
			String sql="insert into newuser(username,password,firstName,lastName,age,dob)values('"+loginmodel.getUsername()+"','"+loginmodel.getPassword()+"','"+loginmodel.getFirstName()+"','"+loginmodel.getLastName()+"','"+loginmodel.getAge()+"','"+loginmodel.getDob()+"')";  
			System.out.println(sql);
			jdbcTemplate.update(sql); 
		}  

	
		public boolean verifyuser(LoginModel loginmodel) {	//for textbox
			
			boolean flag = false;
			try{
			String sql= "select * from newuser";
			
			List<LoginModel> listOfUsers = jdbcTemplate.query(sql, new VerifyUserMapper());
			
	
			for (Iterator<LoginModel> iterator = listOfUsers.iterator(); iterator.hasNext();) 
			{	
				LoginModel loginModel2 = (LoginModel) iterator.next();   //Typecasting
				
				String undb =loginModel2.getUsername();
				String pwdb =loginModel2.getPassword();
				
				if (loginmodel.getUsername().equals(undb) || loginmodel.getPassword().equals(pwdb))
				{
					flag = true;
					break;
				}
				else
				{
					flag= false;
				}
			}
			}
			catch(Exception e)
			{
				System.out.println("Dao error");
			}
			return flag;
		}

		
		public List<TripModel> display() {
		
			String sql= "select * from ticketinfo";
			//System.out.println(sql);
			List<TripModel> listOfUsers = jdbcTemplate.query(sql, new VerifyUserMapper1());
			//System.out.println("got result");
			return listOfUsers;
		}
		
	
		public List<TripModel> displaytickets() {
			String sql= "select * from ticketinfo";
			//System.out.println(sql);
			List<TripModel> listOfUsers = jdbcTemplate.query(sql, new VerifyUserMapper1());
			//System.out.println("got result");
			return listOfUsers;
		}
		

		 public DataSource getDataSource() {
				return dataSource;
			}


			public void setDataSource(DataSource dataSource) {
				this.dataSource = dataSource;
			}


			public JdbcTemplate getJdbcTemplate() {
				return jdbcTemplate;
			}


			public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
				this.jdbcTemplate = jdbcTemplate;
			}

			class VerifyUserMapper implements RowMapper<LoginModel>
			{
				public LoginModel mapRow(ResultSet rs, int arg1)throws SQLException
				{	//System.out.println("test1"+rs.getString("firstname"));
					LoginModel loginModel =new LoginModel();	
					//for database
					loginModel.setUsername(rs.getString("username"));
					loginModel.setPassword(rs.getString("password"));
					loginModel.setFirstName(rs.getString("firstName"));
					loginModel.setLastName(rs.getString("lastName"));
					loginModel.setAge(rs.getString("age"));
					loginModel.setDob(rs.getString("dob"));
					//System.out.println("testing "+loginModel.getFirstname());
					return loginModel;
				}
			}
			
			
			class VerifyUserMapper1 implements RowMapper<TripModel>
			{
				public TripModel mapRow(ResultSet rs, int arg1)throws SQLException
				{	//System.out.println("test1"+rs.getString("firstname"));
					TripModel tripModel =new TripModel();	
					//for database
					tripModel.setLocation(rs.getString("location"));
					tripModel.setFrom(rs.getString("from"));
					tripModel.setTo(rs.getString("to"));
					tripModel.setStatus(rs.getString("status"));
					tripModel.setPrice(rs.getString("price"));
					//System.out.println("testing "+loginModel.getFirstname());
					return tripModel;
				}
			}


			public void update(TripModel tripModel, String mailid) {
				String sql = "Insert into userstatus (username,location,price) values('"+mailid+"','"+tripModel.getLocation()+"', '"+tripModel.getPrice()+"')";
				System.out.println(sql);
				jdbcTemplate.update(sql); 
				
			}


			
			class VerifyUserMapper2 implements RowMapper<CustomerStatusModel>
			{
				public CustomerStatusModel mapRow(ResultSet rs, int arg1)throws SQLException
				{	//System.out.println("test"+rs.getString("getNoOfBooks"));
					
					CustomerStatusModel customerStatusModel =new CustomerStatusModel();	
					//for database
					
					customerStatusModel.setUsername(rs.getString("username"));
					customerStatusModel.setLocation(rs.getString("location"));
					customerStatusModel.setPrice(rs.getString("price"));
					//System.out.println("test"+userStatusModel.getNoOfBooks());
					return customerStatusModel;
				}
			}



			public List userpurchasedisplay(String username) {
				String sql= "select * from userstatus where username='"+username+"'";
				//String sql = "select * from userstatus where username ='"+userStatusModel.getUsername()+"' AND noOfBooks = '"+userStatusModel.getNoOfBooks()+"'";
				System.out.println(sql);
				List<CustomerStatusModel> listOfUsers = jdbcTemplate.query(sql, new VerifyUserMapper2());
				System.out.println("dao"+listOfUsers.get(0).getPrice());

				return listOfUsers;
			}
			





		




	}

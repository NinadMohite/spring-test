package com.pkg.facades.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;


import com.pkg.facades.TripFacades;
import com.pkg.models.CustomerStatusModel;
import com.pkg.models.LoginModel;
import com.pkg.models.TripModel;
import com.pkg.services.impl.TripServicesImpl;

public class TripFacadesImpl implements TripFacades{

	
	@Autowired
	TripServicesImpl loginServicesClass;
	
	
	public void registeruser(LoginModel loginmodel) {
		
		loginServicesClass.registeruser(loginmodel);
	}

	
	public boolean verifyuser(LoginModel loginmodel) {
		
		return loginServicesClass.verifyuser(loginmodel);
	}


	
	public List<TripModel> display() {
		List<TripModel> list = loginServicesClass.display(); 
		return list;
	}
	
	public List<TripModel> displaytickets() {
		List<TripModel> list = loginServicesClass.display(); 
		return list;
	}

	public void update(TripModel tripModel, String mailid) {
		loginServicesClass.update(tripModel, mailid);
		
	}


	public List userpurchasedisplay(String username) {
		List<CustomerStatusModel> list = loginServicesClass.userpurchasedisplay(username);
		return list;
	}





	

}

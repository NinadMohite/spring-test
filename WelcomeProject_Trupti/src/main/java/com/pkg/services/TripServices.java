package com.pkg.services;

import java.util.List;

import com.pkg.models.LoginModel;
import com.pkg.models.TripModel;

public interface TripServices {
	
	
	public void registeruser(LoginModel loginmodel);
	
	public boolean verifyuser(LoginModel loginmodel);
	
	public List<TripModel> display();
	
	public List<TripModel> displaytickets();
	
	public void update(TripModel tripModel,String mailid);
	
	public List userpurchasedisplay(String username);
}

package com.pkg.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.pkg.dao.impl.TripDaoImpl;
import com.pkg.models.CustomerStatusModel;
import com.pkg.models.LoginModel;
import com.pkg.models.TripModel;
import com.pkg.services.TripServices;

public class TripServicesImpl implements TripServices {

	@Autowired
	TripDaoImpl loginDaoClass;

	
	public void registeruser(LoginModel loginmodel) {
		
		 loginDaoClass.registeruser(loginmodel);
	}
	

	public boolean verifyuser(LoginModel loginmodel) {
		
		return loginDaoClass.verifyuser(loginmodel);
	}


	@Override
	public List<TripModel> display() {
		List<TripModel> list = loginDaoClass.display();
		return list;
	}



	public List<TripModel> displaytickets() {
		List<TripModel> list = loginDaoClass.display();
		return list;
	}

	public void update(TripModel tripModel, String mailid) {
		loginDaoClass.update(tripModel, mailid);
	}


	public List userpurchasedisplay(String username) {
		List<CustomerStatusModel> list = loginDaoClass.userpurchasedisplay(username);
		return list;
	}


	
	}





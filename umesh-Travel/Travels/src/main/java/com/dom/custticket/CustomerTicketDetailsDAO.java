package com.dom.custticket;

import java.util.ArrayList;

import com.dom.TravelDetail.TravelDetailPojoServices;

public interface CustomerTicketDetailsDAO {
	
	public void insertCustomerTicketDetails(CustomerTicketDataPojoService pojo);
	
	public ArrayList<CustomerTicketDataPojoService> fetchAllData();

}

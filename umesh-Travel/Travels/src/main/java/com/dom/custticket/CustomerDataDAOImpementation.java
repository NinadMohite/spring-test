package com.dom.custticket;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import com.dom.TravelDetail.TravelDetailPojoServices;
import com.mysql.jdbc.Statement;

public class CustomerDataDAOImpementation implements CustomerTicketDetailsDAO {

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public void insertCustomerTicketDetails(CustomerTicketDataPojoService pojo) {
		
		// TODO Auto-generated method stub
		String sqlread = "SELECT * FROM custticket";
		
		String sql = "INSERT INTO custticket " +
				"(id,price,date,totalprice) VALUES (?, ?,?,?)";
		Connection conn = null;

		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, pojo.getCustid());
			ps.setInt(2, pojo.getPrice());
			ps.setString(3, pojo.getDate());
			ps.setInt(4, pojo.getTotalprice());
			
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}
	
	@Override
	public ArrayList<CustomerTicketDataPojoService> fetchAllData() {
		
		ArrayList<CustomerTicketDataPojoService> pojolist = new ArrayList<CustomerTicketDataPojoService>();
		
		String sql = "SELECT * FROM custticket";

		Connection conn = null;

		try {
			
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				
				CustomerTicketDataPojoService pojo = new CustomerTicketDataPojoService();
				pojo.setCustid(rs.getString("id"));
				pojo.setPrice(rs.getInt("price"));
				pojo.setDate(rs.getString("date"));
				pojo.setTotalprice(rs.getInt("totalprice"));
				
				
				pojolist.add(pojo);
				
				
			}
			
			rs.close();
			ps.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
		return pojolist;
	}

	public void insertcheck(ArrayList<CustomerTicketDataPojoService> pojo){
		
		String sqlread = "SELECT * FROM custticket";
		
		
		
		
		
		String sql = "INSERT INTO custticket " +
				"(id,price) VALUES (?, ?)";
		Connection conn = null;

	

		try {
			for (CustomerTicketDataPojoService customerTicketDataPojoService : pojo) {
			conn = dataSource.getConnection();
			
			/*Statement reads = (Statement) conn.createStatement();
			reads.execute(sqlread);*/
			
			
			
			
			PreparedStatement psread = conn.prepareStatement(sqlread);
			
			ResultSet rs = psread.executeQuery();
			if(!rs.next()){
				
				String sqlwrite = "INSERT INTO custticket " +
						"(id,price) VALUES (?, ?)";
				

				try {
					
					PreparedStatement ps = conn.prepareStatement(sqlwrite);
					ps.setString(1, customerTicketDataPojoService.getCustid());
					ps.setInt(2, customerTicketDataPojoService.getPrice());
					
					ps.executeUpdate();
					ps.close();

				} catch (SQLException e) {
					throw new RuntimeException(e);

				} finally {
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {}
					}
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				
			}else{
				PreparedStatement psread1 = conn.prepareStatement(sqlread);
				
				ResultSet rs1 = psread1.executeQuery();
				while(rs1.next()){
					
					if(rs1.getString("id").equals(customerTicketDataPojoService.getCustid())){
						
						System.out.println("if both id are same");
						
					}else{
						
						String sqlwrite = "INSERT INTO custticket " +
								"(id,price) VALUES (?, ?)";
						
						PreparedStatement ps = conn.prepareStatement(sqlwrite);
						ps.setString(1, customerTicketDataPojoService.getCustid());
						ps.setInt(2, customerTicketDataPojoService.getPrice());
						
						ps.executeUpdate();
						ps.close();
						
					}
					
					
				}
				
				
				
			}
			
			
		}
			

		} catch (SQLException e) {
			throw new RuntimeException(e);

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}

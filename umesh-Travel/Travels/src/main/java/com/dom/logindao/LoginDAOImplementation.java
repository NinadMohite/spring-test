package com.dom.logindao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;



public class LoginDAOImplementation implements LoginDAO {

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public ArrayList<CustomerDetailPojo> login(String name) {
		// TODO Auto-generated method stub
		
		ArrayList<CustomerDetailPojo> pojolist = new ArrayList<CustomerDetailPojo>();
		String sql = "SELECT * FROM custdetails WHERE name = ?";

		Connection conn = null;

		try {
			System.out.println("In Dao" + name);
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, name);
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				CustomerDetailPojo pojo = new CustomerDetailPojo();
				pojo.setCustid(rs.getInt("custid"));
				pojo.setName(rs.getString("name"));
				pojo.setPassword(rs.getString("password"));
				pojo.setCity(rs.getString("city"));
				
				pojolist.add(pojo);
				
				System.out.println(rs.getString("name"));
			}
			
			rs.close();
			ps.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
		return pojolist;
	}
		
		
		
		
		

}

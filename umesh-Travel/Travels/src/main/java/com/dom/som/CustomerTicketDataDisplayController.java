package com.dom.som;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dom.custticket.CustomerDataDAOImpementation;
import com.dom.custticket.CustomerTicketDataPojoService;

@Controller
public class CustomerTicketDataDisplayController {

	@Autowired
	//LoginDAOImplementation tickeDAO;
	CustomerDataDAOImpementation tickeDAO;
	@RequestMapping(value = "/showdata", method = RequestMethod.GET)
	public String fetchAllData(Model m , HttpServletRequest request ,HttpSession s){
		ArrayList<CustomerTicketDataPojoService>  data = tickeDAO.fetchAllData();
		
		if(data != null){
			
			
			
		m.addAttribute("bdata", data);
		request.setAttribute("finaldata", data);
		System.out.println("not null" + data.size());
		
		return "ticketDetails";
		
		}else{
			System.out.println("Not present");
			m.addAttribute("No Book present", data);
			request.setAttribute("No Book present", data);
			return "ticketDetails";
		}
		
	
		
		
	}
	
}

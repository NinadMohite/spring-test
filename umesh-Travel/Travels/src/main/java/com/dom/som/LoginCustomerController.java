package com.dom.som;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dom.logindao.CustomerDetailPojo;
import com.dom.logindao.LoginDAOImplementation;

@Controller
public class LoginCustomerController {
	@Autowired
	LoginDAOImplementation loginDAO;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model m,HttpServletRequest request,HttpSession session){
		
		
		
				int flag = 0;
				String loginName = request.getParameter("name");
				String pass = request.getParameter("pass");
				
				int CustID = 0;
				
				System.out.println("Text field name" + loginName + "  " + pass);
				
				ArrayList<CustomerDetailPojo>  pojo = loginDAO.login(loginName);
				System.out.println("size " + pojo.size());
				if(pojo.size() == 0){
					flag=0;
					System.out.println("In Aray size is 0");
					request.setAttribute("loginName","error.. Invalid..!!");
					m.addAttribute("loginName","User Not Present Please Sign Up");
				}else{
				for (CustomerDetailPojo items : pojo) {
					if(loginName.equals(items.getName()) && pass.equals(items.getPassword())){
						flag = 1;
						loginName = items.getName();
						CustID = items.getCustid();
						session.setAttribute("CustID", CustID);
						
					}
					else {
						System.out.println("In Else  block");
						flag = 0;
						request.setAttribute("loginName","error.. Invalid..!!");
						m.addAttribute("loginName","error.. Invalid..!! User name and password");
						
						
					}
				}
			}
				if(flag==1){
					
					return "redirect:getdata";
					
				}
				else{
					
					return "error";
				}
				
		
		//return null;
		
	}

}

package com.dom.som;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dom.TravelDetail.TravelDetailPojoServices;
import com.dom.custticket.CustomerDataDAOImpementation;
import com.dom.custticket.CustomerTicketDataPojoService;


@Controller
public class CustomeTicketDetailsController {

	@Autowired
	//LoginDAOImplementation tickeDAO;
	CustomerDataDAOImpementation tickeDAO;
	
	@RequestMapping(value = "/save", method = RequestMethod.GET)
	public String saveData(Model m , HttpServletRequest request ,HttpSession s){
		String data[] = request.getParameterValues("data");
		
		if(data == null){
			System.out.println("Please Select One Of Option");
			return "select";
		}else{
		
		ArrayList<CustomerTicketDataPojoService> ListOfActiveBusesData = new ArrayList<CustomerTicketDataPojoService>();
		int totalPrice = 0;
		int custID = (Integer) request.getSession().getAttribute("CustID");
		
		
		long millis = System.currentTimeMillis();
		
		
		
		Date date = new Date(millis);
		
		System.out.println("Date  " + date +" "+  millis);
		
		for (String string : data) {
			
			CustomerTicketDataPojoService customerTicketDataPojoService = new CustomerTicketDataPojoService();
			
			
			String eachitem[] = string.split(" ");
			
			String id =custID  +"-"+eachitem[0] +"-"+ date;
			String aa = eachitem[3];
			
			int price = Integer.parseInt(aa);
			totalPrice = totalPrice + price;
			
			
			
			customerTicketDataPojoService.setCustid(id);
			customerTicketDataPojoService.setPrice(price);
			customerTicketDataPojoService.setDate(String.valueOf(date));
			customerTicketDataPojoService.setTotalprice(totalPrice);
			
			ListOfActiveBusesData.add(customerTicketDataPojoService);
			
			
		}
		
		/*ArrayList<CustomerTicketDataPojoService> dataread = tickeDAO.fetchAllData();
		if(dataread.isEmpty()){
			System.out.println("yes empty");
			
			for(CustomerTicketDataPojoService PojoServices : ListOfActiveBusesData){
				
				//tickeDAO.insertCustomerTicketDetails(PojoServices);
			}
			
		}else{
			
			System.out.println("not empty");
			
			for (CustomerTicketDataPojoService customerTicketDataPojoService : dataread) {
				
				for(CustomerTicketDataPojoService PojoServices : ListOfActiveBusesData){
					
					if(PojoServices.getCustid().equals(customerTicketDataPojoService.getCustid())){
						
					}else{
						CustomerTicketDataPojoService finaldata = new CustomerTicketDataPojoService();
						finaldata.setCustid(PojoServices.getCustid());
						finaldata.setPrice(PojoServices.getPrice());
						
						//tickeDAO.insertCustomerTicketDetails(finaldata);
					}
					
				}
			
		}
		}*/
		/*
			
		}*/
		
		for (CustomerTicketDataPojoService PojoServices : ListOfActiveBusesData) {
			
			
			System.out.println("list values   " + PojoServices.getCustid());
			System.out.println("list values  " + PojoServices.getPrice());
			
			
			tickeDAO.insertCustomerTicketDetails(PojoServices);
			
			}
		//
		return "redirect:showdata";
		
		
		}
	}
	
	
	
}

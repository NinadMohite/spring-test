package com.dom.som;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dom.TravelDetail.TravelDAOImplementation;
import com.dom.TravelDetail.TravelDetailPojoServices;




@Controller
public class TravelDetailsController {
	
	@Autowired
	TravelDAOImplementation travelDAO;
	//BookDAOImplementation bookDAO;
	
	@RequestMapping(value = "/getdata", method = RequestMethod.GET)
	public String fetchData(Model m , HttpServletRequest request ,HttpSession s){
	
		ArrayList<TravelDetailPojoServices>  data = travelDAO.getActiveBuses();
		ArrayList<TravelDetailPojoServices> ListOfActiveBusses = new ArrayList<TravelDetailPojoServices>();
		if(data != null){
			
			for(TravelDetailPojoServices items : data){
				
				if(items.getAvailability().equals("Active")){
					TravelDetailPojoServices activeBuses = new TravelDetailPojoServices();
					activeBuses.setSourcetoDest(items.getSourcetoDest());
					activeBuses.setSource(items.getSource());
					activeBuses.setDestination(items.getDestination());
					activeBuses.setAvailability(items.getAvailability());
					activeBuses.setPrice(items.getPrice());
					
					ListOfActiveBusses.add(activeBuses);
				}
				
				
				
				/*System.out.println(aa.getNameOfBook());
				System.out.println(aa.getAuthor());
				System.out.println(aa.getPice());*/
			}
			
		m.addAttribute("bdata", ListOfActiveBusses);
		request.setAttribute("bsdata", ListOfActiveBusses);
		System.out.println("not null" + data.size());
		
		return "travelsDetails";
		
		}else{
			System.out.println("Not present");
			m.addAttribute("No Book present", data);
			request.setAttribute("No Book present", data);
			return "travelsDetails";
		}
		
	}

}

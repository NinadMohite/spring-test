package com.dom.TravelDetail;

public class TravelDetailPojoServices {
	private String sourcetoDest;
	private String source;
	private String destination;
	private String availability;
	private int price;
	
	
	public String getSourcetoDest() {
		return sourcetoDest;
	}
	public void setSourcetoDest(String sourcetoDest) {
		this.sourcetoDest = sourcetoDest;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	
}

package com.dom.TravelDetail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;





public class TravelDAOImplementation implements TravelDAO {

	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public ArrayList<TravelDetailPojoServices> getActiveBuses() {
		// TODO Auto-generated method stub
		ArrayList<TravelDetailPojoServices> pojolist = new ArrayList<TravelDetailPojoServices>();
		
		String sql = "SELECT * FROM traveldetails";

		Connection conn = null;

		try {
			
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				TravelDetailPojoServices pojo = new TravelDetailPojoServices();
				pojo.setSourcetoDest(rs.getString("SD"));
				pojo.setSource(rs.getString("source"));
				pojo.setDestination(rs.getString("destination"));
				pojo.setAvailability(rs.getString("availability"));
				pojo.setPrice(rs.getInt("price"));
				
				pojolist.add(pojo);
				
				
			}
			
			rs.close();
			ps.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
		return pojolist;
	}

}
